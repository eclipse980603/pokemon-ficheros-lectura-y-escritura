package program;

import java.util.Scanner;

public class PokemonTest {
	public static String PATH = "C:/Users/Usuario/eclipse-workspace/POKEMON/CSVS/";
	public static String FILE = "pokemons.csv";
	public static String FILE_TOTALS = "pokemonsTotals.csv";
	public static String FILE_SEARCH = "pokemonsSearch.csv";
	public static String FILE_GENERATION = "pokemonsGenerations.csv";
	public static String FILE_LEGENDARY = "pokemonsLegendary.csv";
	public static String FILE_GEN_NAME = "pokemonsGenName.csv";
	public static String FILE_TYPE = "pokemonsType.csv";
	public static String FILE_EXT = ".csv";
	
	PokemonTest(){
		super();
	}
	
	public static void main(String[] args) {
		Scanner leer = new Scanner(System.in);
		FileManager fileManager = new FileManager();
		boolean bucle = true;
		while(bucle) {
			menu();
			switch(leer.nextLine()) {
			case "1":
				System.out.print("CHOSE MENU OPTION: 1 \n" + "TOTALS \n" + "ENETER TYPE1 (ENTER FOR ALL):");
				String type1 = leer.nextLine();
				System.out.print("\nENETER TYPE2 (ENTER FOR ALL):");
				String type2 = leer.nextLine();
				String text = fileManager.readTotals(PATH + FILE, type1, type2);
				if(text != "") {
					fileManager.write(text, PATH + FILE_TOTALS);
					System.out.println("FILE" + FILE_TOTALS + " CREATED.");
				}else {
					if(fileManager.deleteFile(PATH + FILE_TOTALS)) {
						System.out.println("FILE" + FILE_TOTALS + " DELETED. THERE ARE NO POKEMONS WITH THIS SPECIFICATIONS.");
					}else {
						System.out.println("FILE" + FILE_TOTALS + " IS OPENED AND CAN'T BE DELETED. "
								+ "THERE ARE NO POKEMONS WITH THIS SPECIFICATIONS.");
					}
				}
				break;
			case "2":
				System.out.print("CHOSE MENU OPTION: 2 \n" + "POKEMON SEARCH \n" + "ENETER NAME:");
				String name = leer.nextLine();
				break;
			case "3":
				System.out.print("CHOSE MENU OPTION: 3 \n" + "EXTRACT GENERATION \n" + "CHOOSE GENERATION:");
				String generation = leer.nextLine();
				break;
			case "4":
				System.out.print("CHOSE MENU OPTION: 4 \n" + "ORDER LEGENDARY \n" + "CHOOSE LEGENDARY 1.TRUE, 2.FALSE");
				String legendary = leer.nextLine();
				break;
			case "5":
				System.out.print("CHOSE MENU OPTION: 5 \n" + "ORDER BY TYPE1 AND TYPE2 \n" + "ENETER TYPE1 AND TYPE2:");
				String types = leer.nextLine();
				break;
			case "6":
				System.out.print("CHOSE MENU OPTION: 6 \n" + "ORDER BY GENERATION AND NAME \n");
				String genName = leer.nextLine();
				break;
			case "0":
				System.out.print("CHOSE MENU OPTION: 0 \n" + "QUIT \n" + "GOOD BYE");
				bucle = false;
				break;
			default:
				System.out.println("WRITE A CORRECT OPTION");
				break;
			}
		}

		
	}
	
	public static void menu() {
		System.out.println("MENU \n"
						 + "1. TOTALS \n"
						 + "2. POKEMON SEARCH \n"
						 + "3. EXTRACT GENERATION \n"
						 + "4. ORDER LEGENDARY \n"
						 + "5. ORDER BY TYPE1 AND TYPE2 \n"
						 + "6. ORDER BY GENERATION AND NAME \n"
						 + "0. QUIT \n"
						 + "CHOSE MENU OPTION:");
	}
	
}
