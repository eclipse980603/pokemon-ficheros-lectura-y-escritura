package program;

import java.util.Comparator;

public class ComparatorGenerationName implements Comparator<Pokemon> {
	
	ComparatorGenerationName(){}
	
	@Override
	public int compare(Pokemon p1, Pokemon p2) {
		int generation = Byte.compare(p1.getGeneration(), p2.getGeneration());
		int name = p1.getName().compareTo(p2.getName());
		return (generation == 0) ? name : generation;
	}

}
