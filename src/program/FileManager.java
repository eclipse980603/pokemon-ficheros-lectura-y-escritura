package program;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;

public class FileManager {

	FileManager(){
		super();
	}
	
	public String readTotals(String fileInput, String type1, String type2) {
		Scanner leer = new Scanner(System.in);
		StringBuffer text = new StringBuffer();
		int total = 0;
		File pokemons = new File(fileInput);
		String header = "Code;Name;Type1;Type2;Total\n";
		try(FileReader fr = new FileReader (pokemons)){
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			line = br.readLine();
			line = br.readLine();
			while(line!=null) {
	            String[] fields = splitLine(line);
	            if(fields[2].toUpperCase().contains(type1.toUpperCase()) &&
	            		fields[3].toUpperCase().contains(type2.toUpperCase())) {
	            	for (int i = 4; i <=9;i++) {
	            		total += Integer.parseInt(fields[i]);
	            	}
	            	text.append(fields[0] + ";" +
	            				fields[1] + ";" +
	            				fields[2] + ";" +
	            				fields[3] + ";");
	            	text.append(total + "\n");
	            	total = 0;
	            }
	            line = br.readLine();
	         }
			
	         br.close();

		} catch (FileNotFoundException e) {
			System.out.println("File Not Found: " + e.getMessage());
	 	}catch (IOException e){
			System.out.println("Error: " + e.getMessage());
		}
		
		if(text.toString().isEmpty()) return "";
		else return header + text.toString();
	}
	
	public void write(String text, String fileOutput){
		try {
			FileWriter fw = new FileWriter(fileOutput, false);
			BufferedWriter bw = new BufferedWriter(fw);
			fw.write(text);
			fw.close();
			bw.close();
		}catch (Exception e){
			 e.printStackTrace();
		}
	}
	
	
	
	public List<Pokemon> searchPokemons(String fileInput, String pokemonName){
		
		return null;
		
	}
	
	
	
	public void write(List<Pokemon> pokemons, String fileOutput) {
		
	}
	
	
	
	public List<Pokemon> extractGeneration(String fileInput, byte generation){
		return null;
		
	}
	
	
	
	public List<Pokemon> orderLegendary(String fileInput, byte legendary){
		return null;
		
	}
	
	
	

	public List<Pokemon> orderType(String fileInput){
		return null;
		
	}
	
	
	

	public List<Pokemon> orderGenerationName(String fileInput){
		return null;
		
	}
	
	public boolean deleteFile(String fileInput) {
		File file = new File(fileInput);
        
        if (file.exists()) {
            if (file.delete()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	
	public String[] splitLine(String line) {
		String [] splited = line.split(";");
		return splited;
		
	}
	
}
