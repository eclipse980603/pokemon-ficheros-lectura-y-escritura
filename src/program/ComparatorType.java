package program;

import java.util.Comparator;

public class ComparatorType implements Comparator<Pokemon> {

	ComparatorType(){}
	

	@Override
	public int compare(Pokemon p1, Pokemon p2) {
		int type1 = p1.getType1().compareTo(p2.getType1());
		int type2 = p1.getType2().compareTo(p2.getType2());
		return (type1 == 0) ? type2 : type1;
	}

}