package program;

public class Pokemon {
	private int code;
	private int healthPoints;
	private int attack;
	private int defense;
	private int specialAttack;
	private int specialDefense;
	private int speed;
	private String name;
	private String type1;
	private String type2;
	private byte generation;
	private boolean legendary;
	
	Pokemon(){super();}
	
	Pokemon(int code, int healthPoints, int attack, int defense, int specialAttack, int specialDefense, int speed
			, String name, String type1, String type2, byte generation, boolean legendary){
		setCode(code);
		setHealthPoints(healthPoints);
		setAttack(attack);
		setDefense(defense);
		setSpecialAttack(specialAttack);
		setSpecialDefense(specialDefense);
		setSpeed(speed);
		setName(name);
		setType1(type1);
		setType2(type2);
		setGeneration(generation);
		setLegendary(legendary);
		
	}
	
	public void setCode(int code){
		this.code = code;
	}
	
	public int getCode(){
		return code;
	}
	
	public void setHealthPoints(int healthPoints){
		this.healthPoints = healthPoints;
	}
	
	public int getHealthPoints(){
		return healthPoints;
	}
	
	public void setAttack(int attack){
		this.attack = attack;
	}
	
	public int getAttack(){
		return attack;
	}
	
	public void setDefense(int defense){
		this.defense = defense;
	}
	
	public int getDefense(){
		return defense;
	}
	
	public void setSpecialAttack(int specialAttack){
		this.specialAttack = specialAttack;
	}
	
	public int getSpecialAttack(){
		return specialAttack;
	}
	
	public void setSpecialDefense(int specialDefense){
		this.specialDefense = specialDefense;
	}
	
	public int getSpecialDefense(){
		return specialDefense;
	}
	
	public void setSpeed(int speed){
		this.speed = speed;
	}
	
	public int getSpeed(){
		return speed;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setType1(String type1){
		this.type1 = type1;
	}
	
	public String getType1(){
		return type1;
	}
	
	
	public void setType2(String type2){
		this.type2 = type2;
	}
	
	public String getType2(){
		return type2;
	}
	
	public void setGeneration(byte generation){
		this.generation = generation;
	}
	
	public byte getGeneration(){
		return generation;
	}
	
	public void setLegendary(boolean legendary){
		this.legendary = legendary;
	}
	
	public boolean getLegendary(){
		return legendary;
	}
	
	@Override
    public String toString() {
        return "Pokemon: \n" +
                "\n code=" + getCode() +
                ",\n healthPoints=" + getHealthPoints() +
                ",\n attack=" + getAttack() +
                ",\n defense=" + getDefense() +
                ",\n specialAttack=" + getSpecialAttack() +
                ",\n specialDefense=" + getSpecialDefense() +
                ",\n speed=" + getSpeed() +
                ",\n name='" + getName() + '\'' +
                ",\n type1='" + getType1() + '\'' +
                ",\n type2='" + getType2() + '\'' +
                ",\n generation=" + getGeneration() +
                ",\n legendary=" + getLegendary();
    }
    
	/*@Override
	public int CompareTo(String pk) {
		
		return 0;
	}*/
    
}
